package main

import (
	"encoding/json"
	"io/ioutil"
	"os"

	irc "github.com/thoj/go-ircevent"
)

// Connections ..
type Connections []Conn

// Conn ..
type Conn struct {
	Channel  string `json:"channel"`
	Address  string `json:"address"`
	Username string `json:"username"`
	Password string `json:"password"`
}

func main() {
	var conns Connections

	cfgBytes, err := ioutil.ReadFile("config.json")
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	if err := json.Unmarshal(cfgBytes, &conns); err != nil {
		println(err.Error())
		os.Exit(1)
	}

	if len(conns) != 2 {
		println("config array must have 2 items")
		os.Exit(1)
	}

	ircs := []*irc.Connection{
		irc.IRC(conns[0].Username, conns[0].Username),
		irc.IRC(conns[1].Username, conns[1].Username),
	}

	for i, conn := range ircs {
		conn.AddCallback("001", func(e *irc.Event) {
			conn.Join(conns[i].Channel)
		})

		conn.Password = conns[i].Password

		if i == 1 {
			i = 0
		} else {
			i = 1
		}

		conn.AddCallback("PRIVMSG", func(e *irc.Event) {
			if e.User == conns[i].Username {
				return
			}

			var name = e.User
			if e.Nick != "" {
				name = e.Nick
			}

			conn.Privmsgf(
				conns[i].Channel,
				"%s: %s",
				name, e.Message(),
			)
		})
	}

	for i, conn := range ircs {
		if err := conn.Connect(conns[i].Address); err != nil {
			println(err.Error())
			os.Exit(1)
		}
	}

	ircs[0].Loop()
	ircs[1].Loop()
}
